# Docker Registry Demo Readme

Steps to run the demo

- Running this command on docker machine (named default) which was assigned IP 192.168.99.100

	docker run -d -p 5000:5000 --name registry registry:2
	
- To allow insecure http connections to the registry

	docker-machine ssh default
	
	sudo vi /mnt/sda1/var/lib/boot2docker/profile
	
Add the --insecure-registry line as below

	--label provider=virtualbox
	--insecure-registry 192.168.99.100:5000

Save the file and exit the ssh connection to default.

	docker-machine restart default

- Re-tag our app images in local cache with registry prefix and a version suffix. Version 1 for this demo

	docker tag nttdatalab/nuggetzbank-transactions 192.168.99.100:5000/nttdatalab/nuggetzbank-transactions:1

	docker tag nttdatalab/nuggetzbank-accounts 192.168.99.100:5000/nttdatalab/nuggetzbank-accounts:1
	
	docker tag nttdatalab/haproxy-project-specific 192.168.99.100:5000/nttdatalab/haproxy-project-specific:1
	
- Push images to the registry

	docker push 192.168.99.100:5000/nttdatalab/nuggetzbank-transactions:1
	
	docker push 192.168.99.100:5000/nttdatalab/nuggetzbank-accounts:1
	
	docker push 192.168.99.100:5000/nttdatalab/haproxy-project-specific:1
	
- Use the registry http api for actions (see full api listing https://docs.docker.com/registry/spec/api/)

	curl -X GET http://192.168.99.100:5000/v2/nttdatalab/nuggetzbank-transactions/tags/list